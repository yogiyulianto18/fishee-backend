<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use SebastianBergmann\GlobalState\Restorer;

class Device extends RestController{
    
    function __construct()
    {
        parent::__construct();
    }

    // function get all device data

    public function index_get(){
        $device = $this->db->get('tbl_device')->result();

        if($device){
            $data_json = [
                "success" => true,
                "message" => "data found",
                "data" => $device
            ];
        }else{
            $data_json = [
                "success" => false,
                "message" => "data not found",
                "data" => null
            ];
        }

        $this->response($data_json, RestController::HTTP_OK);
    }

    public function index_post(){
        $device = json_decode($this->input->raw_input_stream);
        
        $data_insert = [
            "name" => $device->name,
            "token" => bin2hex(random_bytes(20)),
        ]; 

        $insert = $this->db->insert('tbl_device', $data_insert);

        if($insert){
            $data_json = array(
                "success" => true,
                "message" => "input device data succesfull",
                "data" => $data_insert
            );
        }else{
            $data_json = array(
                "success" => false,
                "message" => "input device data failed",
                "data" => null
            );
        }

        $this->response($data_json, RestController::HTTP_OK);
    }

    public function index_put(){
        $device = json_decode($this->input->raw_input_stream);

        $data_update = [
            "name" => $device->name,
            "token" => bin2hex(random_bytes(20))
        ];

        $this->db->where('id', $device->id);
        $this->db->set($data_update);
        $update = $this->db->update('tbl_device');

        if($update){
            $data_json = array(
                "success" => true,
                "message" => "device update succesfully",
                "data" => $data_update
            );
        }else{
            $data_json = array(
                "success" => false,
                "message" => "device update failed",
                "data" => null
            );
        }

        $this->response($data_json, RestController::HTTP_OK);
    }

    public function index_delete($id){
        $this->db->where('id', $id);
        $delete = $this->db->delete('tbl_device');

        if($delete){
            $data_json = array(
                "success" => true,
                "message" => "delete device successfuly"
            );
        }else{
            $data_json = array(
                "success" => false,
                "message" => "delete device failed"
            );
        }

        $this->response($data_json, RestController::HTTP_OK);
    }
}