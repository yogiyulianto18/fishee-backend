<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use SebastianBergmann\GlobalState\Restorer;


class Kolam extends RestController{


    function __construct()
    {
        parent::__construct();
    }

    public function index_get(){
        $kolam = $this->db->get('tbl_kolam')->result();


        if($kolam){
            $data_json = array(
                "success" => true,
                "message" => "data found",
                "data" => $kolam
            );
        }else{
            $data_json = array(
                "success" => false,
                "message" => "data not found",
                "data" => null
            );
        }

        $this->response($data_json, RestController::HTTP_OK);

    }

    public function index_post(){
        $kolam = json_decode($this->input->raw_input_stream);

        $data_insert = [
            "name" => $kolam->name,
            "panjang" => $kolam->panjang,
            "alamat" => $kolam->alamat,
            "luas" => $kolam->luas,
            "latitude" => $kolam->latitude,
            "longitude" => $kolam->longitude,
        ];

        $insert = $this->db->insert('tbl_kolam', $data_insert);

        if($insert){
            $data_json = array(
                "success" => true,
                "message" => "input kolam data successfuly",
                "data" => $data_insert
            );
        }else{
            $data_json = array(
                "success" => false,
                "message" => "input kolam data failed",
                "data" => null
            );
        }

        $this->response($data_json, RestController::HTTP_OK);
    }

    public function index_put($id){
        $kolam = json_decode($this->input->raw_input_stream);

        $data_update = [
            "name" => $kolam->name,
            "panjang" => $kolam->panjang,
            "alamat" => $kolam->alamat,
            "luas" => $kolam->luas,
            "latitude" => $kolam->latitude,
            "longitude" => $kolam->longitude,
        ];

        $this->db->where('id', $id);
        $this->db->set($data_update);
        $update = $this->db->update('tbl_kolam');

        if($update){
            $data_json = array(
                "success" => true,
                "message" => "update kolam data successfuly",
                "data" => $data_update
            );
        }else{
            $data_json = array(
                "success" => false,
                "message" => "update kolam data failed",
                "data" => null
            );
        }

        $this->response($data_json, RestController::HTTP_OK);

    }

    public function index_delete($id){
        $this->db->where('id', $id);
        $delete = $this->db->delete('tbl_kolam');

        if($delete){
            $data_json = array(
                "success" => true,
                "message" => "delete kolam data successfuly"
            );
        }else{
            $data_json = array(
                "success" => false,
                "message" => "delete kolam data failed",
            );
        }

        $this->response($data_json, RestController::HTTP_OK);
    }

    public function test_add1(){
        echo "Test add ci/cd pipeline";
    }

    // testing deploy
    
}