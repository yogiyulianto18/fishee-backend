<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use SebastianBergmann\GlobalState\Restorer;


class Template_Pakan extends RestController{

    function __construct()
    {
        parent::__construct();
    }

    public function index_get(){
        $template_pakan = $this->db->get('tbl_template_pakan')->result();

        if($template_pakan){
            $data_json = [
                "success" => true,
                "message" => "data found",
                "data" => $template_pakan
            ];
        }else{
            $data_json = [
                "success" => false,
                "message" => "data not found",
                "data" => null
            ];
        }

        $this->response($data_json, RestController::HTTP_OK);
    }

    public function index_post(){
        $template_pakan = json_decode($this->input->raw_input_stream);

        $data_insert = [
            "name" => $template_pakan->name,
            "sni_no" => $template_pakan->sni_no,
            "ikan_id" => $template_pakan->ikan_id,
            "user_id" => $template_pakan->user_id,
            "type" => $template_pakan->type
        ];

        $insert = $this->db->insert('tbl_template_pakan', $data_insert);

        if($insert){
            $data_json = array(
                "success" => true,
                "message" => "insert template pakan data successfuly",
                "data" => $data_insert
            );
        }else{
            $data_json = array(
                "success" => false,
                "message" => "insert template pakan data failed",
                "data" => null
            );
        }

        $this->response($data_json, RestController::HTTP_OK);
    }

    public function index_put($id){
        $template_pakan = json_decode($this->input->raw_input_stream);

        $data_update = [
            "name" => $template_pakan->name,
            "sni_no" => $template_pakan->sni_no,
            "ikan_id" => $template_pakan->ikan_id,
            "user_id" => $template_pakan->user_id,
            "type" => $template_pakan->type
        ];

        $this->db->where('id', $id);
        $this->db->set($data_update);
        $update = $this->db->update('tbl_template_pakan');

        if($update){
            $data_json = array(
                "success" => true,
                "message" => "update template pakan data successfuly",
                "data" => $data_update
            );
        }else{
            $data_json = array(
                "success" => false,
                "message" => "update template pakan data failed",
                "data" => null
            );
        }

        $this->response($data_json, RestController::HTTP_OK);
    }

    public function index_delete($id){
        $this->db->where('id', $id);
        $delete = $this->db->delete('tbl_template_pakan');

        if($delete){
            $data_json = array(
                "success" => true,
                "message" => "delete template pakan successfuly"
            );  
        }else{
            $data_json = array(
                "success" => false,
                "message" => "delete template pakan failed"
            );  
        }

        $this->response($data_json, RestController::HTTP_OK);
    }

}