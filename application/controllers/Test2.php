
<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use SebastianBergmann\GlobalState\Restorer;

////
class Test2 extends RestController
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function index_get(){
        $user = $this->db->get('tbl_ikan')->result();


        if($user){
            $data_json = array(
                "success" => true,
                "message" => "data found",
                "data" => $user
            );
        }else{
            $data_json = array(
                "success" => false,
                "message" => "data not found",
                "data" => null
            );
        }

        $this->response($data_json, RestController::HTTP_OK);
    }


    public function index_post(){
        // validation for name, photo
        $this->form_validation->set_rules('name', 'Nama Ikan', 'required');
        // validation
        if ($this->form_validation->run() == FALSE) {
            $data_json = array(
                "success" => false,
                "message" => "data not found",
                "data" => null
            );
            $this->response($data_json, RestController::HTTP_OK);
        } else {
            // validation image ftp post if empty
            if (empty($_FILES['photo']['name'])) {
                $this->form_validation->set_rules('photo', 'Photo', 'required');
            }
            // upload image
            $config['upload_path'] = './assets/images/ikan/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 2048;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            // upload image
            if ($this->upload->do_upload('photo')) {
                $data = $this->upload->data();
                $image = $data['file_name'];
            } else {
                $image = null;
            }
            $data_insert = [
                "nama_ikan" => $this->post('name'),
                "photo" => $image,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ];

            $insert = $this->db->insert('tbl_ikan',$data_insert);
            if($insert){
                $data_json = array(
                    "success" => true,
                    "message" => "data found",
                    "data" => $data_insert
                );
            }else{
                $data_json = array(
                    "success" => false,
                    "message" => "data not found",
                    "data" => null
                );
            }
            $this->response($data_json, RestController::HTTP_OK);
        }
        
    }

    public function index_put($id){
        // validation for name, photo
        $this->form_validation->set_rules('name', 'Nama Ikan', 'required');
        // validation
        if ($this->form_validation->run() == FALSE) {
            $data_json = array(
                "success" => false,
                "message" => "data not found",
                "data" => null
            );
            $this->response($data_json, RestController::HTTP_OK);
        } else {
            // upload image
            $config['upload_path'] = './assets/images/ikan/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 2048;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            // upload image
            if ($this->upload->do_upload('photo')) {
                $data = $this->upload->data();
                $image = $data['file_name'];
            } else {
                $image = null;
            }
            $data_update = [
                "nama_ikan" => $this->put('name'),
                "photo" => $image,
                "updated_at" => date('Y-m-d H:i:s')
            ];

            $this->db->where('id',$id);
            $update = $this->db->update('tbl_ikan',$data_update);

            if($update){
                $data_json = array(
                    "success" => true,
                    "message" => "ikan update success",
                    "data" => $data_update
                );
            }else{
                $data_json = array(
                    "success" => false,
                    "message" => "ikan update fail",
                    "data" => $data_update
                );
            }

            $this->response($data_json, RestController::HTTP_OK);
        }
    }

    public function index_delete($id){

        $this->db->where('id', $id);
        $delete = $this->db->delete('tbl_ikan');
       
        if($delete){
            $data_json = array(
                "success" => true,
                "message" => "delete user success"
            );
        }else{
            $data_json = array(
                "success" => false,
                "message" => "delete user fail"
            );
        }

        $this->response($data_json, RestController::HTTP_OK);
    }

    public function index_post(){
        // validation for name, photo
        $this->form_validation->set_rules('name', 'Nama Ikan', 'required');
        // validation
        if ($this->form_validation->run() == FALSE) {
            $data_json = array(
                "success" => false,
                "message" => "data not found",
                "data" => null
            );
            $this->response($data_json, RestController::HTTP_OK);
        } else {
            // validation image ftp post if empty
            if (empty($_FILES['photo']['name'])) {
                $this->form_validation->set_rules('photo', 'Photo', 'required');
            }
            // upload image
            $config['upload_path'] = './assets/images/ikan/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 2048;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            // upload image
            if ($this->upload->do_upload('photo')) {
                $data = $this->upload->data();
                $image = $data['file_name'];
            } else {
                $image = null;
            }
            $data_insert = [
                "nama_ikan" => $this->post('name'),
                "photo" => $image,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ];

            $insert = $this->db->insert('tbl_ikan',$data_insert);
            if($insert){
                $data_json = array(
                    "success" => true,
                    "message" => "data found",
                    "data" => $data_insert
                );
            }else{
                $data_json = array(
                    "success" => false,
                    "message" => "data not found",
                    "data" => null
                );
            }
            $this->response($data_json, RestController::HTTP_OK);
        }
        
    }

    public function index_put($id){
        // validation for name, photo
        $this->form_validation->set_rules('name', 'Nama Ikan', 'required');
        // validation
        if ($this->form_validation->run() == FALSE) {
            $data_json = array(
                "success" => false,
                "message" => "data not found",
                "data" => null
            );
            $this->response($data_json, RestController::HTTP_OK);
        } else {
            // upload image
            $config['upload_path'] = './assets/images/ikan/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 2048;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            // upload image
            if ($this->upload->do_upload('photo')) {
                $data = $this->upload->data();
                $image = $data['file_name'];
            } else {
                $image = null;
            }
            $data_update = [
                "nama_ikan" => $this->put('name'),
                "photo" => $image,
                "updated_at" => date('Y-m-d H:i:s')
            ];

            $this->db->where('id',$id);
            $update = $this->db->update('tbl_ikan',$data_update);

            if($update){
                $data_json = array(
                    "success" => true,
                    "message" => "ikan update success",
                    "data" => $data_update
                );
            }else{
                $data_json = array(
                    "success" => false,
                    "message" => "ikan update fail",
                    "data" => $data_update
                );
            }

            $this->response($data_json, RestController::HTTP_OK);
        }
    }

    public function index_delete($id){

        $this->db->where('id', $id);
        $delete = $this->db->delete('tbl_ikan');
       
        if($delete){
            $data_json = array(
                "success" => true,
                "message" => "delete user success"
            );
        }else{
            $data_json = array(
                "success" => false,
                "message" => "delete user fail"
            );
        }

        $this->response($data_json, RestController::HTTP_OK);
    }

    public function index_post(){
        // validation for name, photo
        $this->form_validation->set_rules('name', 'Nama Ikan', 'required');
        // validation
        if ($this->form_validation->run() == FALSE) {
            $data_json = array(
                "success" => false,
                "message" => "data not found",
                "data" => null
            );
            $this->response($data_json, RestController::HTTP_OK);
        } else {
            // validation image ftp post if empty
            if (empty($_FILES['photo']['name'])) {
                $this->form_validation->set_rules('photo', 'Photo', 'required');
            }
            // upload image
            $config['upload_path'] = './assets/images/ikan/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 2048;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            // upload image
            if ($this->upload->do_upload('photo')) {
                $data = $this->upload->data();
                $image = $data['file_name'];
            } else {
                $image = null;
            }
            $data_insert = [
                "nama_ikan" => $this->post('name'),
                "photo" => $image,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ];

            $insert = $this->db->insert('tbl_ikan',$data_insert);
            if($insert){
                $data_json = array(
                    "success" => true,
                    "message" => "data found",
                    "data" => $data_insert
                );
            }else{
                $data_json = array(
                    "success" => false,
                    "message" => "data not found",
                    "data" => null
                );
            }
            $this->response($data_json, RestController::HTTP_OK);
        }
        
    }

    public function index_put($id){
        // validation for name, photo
        $this->form_validation->set_rules('name', 'Nama Ikan', 'required');
        // validation
        if ($this->form_validation->run() == FALSE) {
            $data_json = array(
                "success" => false,
                "message" => "data not found",
                "data" => null
            );
            $this->response($data_json, RestController::HTTP_OK);
        } else {
            // upload image
            $config['upload_path'] = './assets/images/ikan/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 2048;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            // upload image
            if ($this->upload->do_upload('photo')) {
                $data = $this->upload->data();
                $image = $data['file_name'];
            } else {
                $image = null;
            }
            $data_update = [
                "nama_ikan" => $this->put('name'),
                "photo" => $image,
                "updated_at" => date('Y-m-d H:i:s')
            ];

            $this->db->where('id',$id);
            $update = $this->db->update('tbl_ikan',$data_update);

            if($update){
                $data_json = array(
                    "success" => true,
                    "message" => "ikan update success",
                    "data" => $data_update
                );
            }else{
                $data_json = array(
                    "success" => false,
                    "message" => "ikan update fail",
                    "data" => $data_update
                );
            }

            $this->response($data_json, RestController::HTTP_OK);
        }
    }

    public function index_delete($id){

        $this->db->where('id', $id);
        $delete = $this->db->delete('tbl_ikan');
       
        if($delete){
            $data_json = array(
                "success" => true,
                "message" => "delete user success"
            );
        }else{
            $data_json = array(
                "success" => false,
                "message" => "delete user fail"
            );
        }

        $this->response($data_json, RestController::HTTP_OK);
    }

    public function index_post(){
        // validation for name, photo
        $this->form_validation->set_rules('name', 'Nama Ikan', 'required');
        // validation
        if ($this->form_validation->run() == FALSE) {
            $data_json = array(
                "success" => false,
                "message" => "data not found",
                "data" => null
            );
            $this->response($data_json, RestController::HTTP_OK);
        } else {
            // validation image ftp post if empty
            if (empty($_FILES['photo']['name'])) {
                $this->form_validation->set_rules('photo', 'Photo', 'required');
            }
            // upload image
            $config['upload_path'] = './assets/images/ikan/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 2048;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            // upload image
            if ($this->upload->do_upload('photo')) {
                $data = $this->upload->data();
                $image = $data['file_name'];
            } else {
                $image = null;
            }
            $data_insert = [
                "nama_ikan" => $this->post('name'),
                "photo" => $image,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ];

            $insert = $this->db->insert('tbl_ikan',$data_insert);
            if($insert){
                $data_json = array(
                    "success" => true,
                    "message" => "data found",
                    "data" => $data_insert
                );
            }else{
                $data_json = array(
                    "success" => false,
                    "message" => "data not found",
                    "data" => null
                );
            }
            $this->response($data_json, RestController::HTTP_OK);
        }
        
    }

    public function index_put($id){
        // validation for name, photo
        $this->form_validation->set_rules('name', 'Nama Ikan', 'required');
        // validation
        if ($this->form_validation->run() == FALSE) {
            $data_json = array(
                "success" => false,
                "message" => "data not found",
                "data" => null
            );
            $this->response($data_json, RestController::HTTP_OK);
        } else {
            // upload image
            $config['upload_path'] = './assets/images/ikan/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 2048;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            // upload image
            if ($this->upload->do_upload('photo')) {
                $data = $this->upload->data();
                $image = $data['file_name'];
            } else {
                $image = null;
            }
            $data_update = [
                "nama_ikan" => $this->put('name'),
                "photo" => $image,
                "updated_at" => date('Y-m-d H:i:s')
            ];

            $this->db->where('id',$id);
            $update = $this->db->update('tbl_ikan',$data_update);

            if($update){
                $data_json = array(
                    "success" => true,
                    "message" => "ikan update success",
                    "data" => $data_update
                );
            }else{
                $data_json = array(
                    "success" => false,
                    "message" => "ikan update fail",
                    "data" => $data_update
                );
            }

            $this->response($data_json, RestController::HTTP_OK);
        }
    }

    public function index_delete($id){

        $this->db->where('id', $id);
        $delete = $this->db->delete('tbl_ikan');
       
        if($delete){
            $data_json = array(
                "success" => true,
                "message" => "delete user success"
            );
        }else{
            $data_json = array(
                "success" => false,
                "message" => "delete user fail"
            );
        }

        $this->response($data_json, RestController::HTTP_OK);
    }

    

    

}
